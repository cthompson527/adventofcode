package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	partOne()
	partTwo()
}

/*
--- Day 2: I Was Told There Would Be No Math ---
The elves are running low on wrapping paper, and so they need to submit an order for more. They have a list of the dimensions (length l, width w, and height h) of each present, and only want to order exactly as much as they need.

Fortunately, every present is a box (a perfect right rectangular prism), which makes calculating the required wrapping paper for each gift a little easier: find the surface area of the box, which is 2*l*w + 2*w*h + 2*h*l. The elves also need a little extra paper for each present: the area of the smallest side.

For example:

A present with dimensions 2x3x4 requires 2*6 + 2*12 + 2*8 = 52 square feet of wrapping paper plus 6 square feet of slack, for a total of 58 square feet.
A present with dimensions 1x1x10 requires 2*1 + 2*10 + 2*10 = 42 square feet of wrapping paper plus 1 square foot of slack, for a total of 43 square feet.
All numbers in the elves' list are in feet. How many total square feet of wrapping paper should they order?
*/

func partOne() {
	f, err := os.Open("input.txt")
	if err != nil {
		log.Fatal(err)
	}

	total := 0
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		vals := strings.Split(line, "x")
		valOne, errOne := strconv.ParseInt(vals[0], 10, 32)
		valTwo, errTwo := strconv.ParseInt(vals[1], 10, 32)
		valThr, errThr := strconv.ParseInt(vals[2], 10, 32)

		if errOne != nil || errTwo != nil || errThr != nil {
			log.Printf("%v\n", errOne)
			log.Printf("%v\n", errTwo)
			log.Printf("%v\n", errThr)
			log.Fatal("Error")
		}
		firstTwo := valOne * valTwo
		secondTwo := valTwo * valThr
		firstLast := valOne * valThr

		slack := firstTwo
		if secondTwo < slack {
			slack = secondTwo
		}
		if firstLast < slack {
			slack = firstLast
		}

		total += int(2*firstTwo + 2*secondTwo + 2*firstLast + slack)
	}

	fmt.Printf("Total area: %v\n", total)
}

/*
--- Part Two ---
The elves are also running low on ribbon. Ribbon is all the same width, so they only have to worry about the length they need to order, which they would again like to be exact.

The ribbon required to wrap a present is the shortest distance around its sides, or the smallest perimeter of any one face. Each present also requires a bow made out of ribbon as well; the feet of ribbon required for the perfect bow is equal to the cubic feet of volume of the present. Don't ask how they tie the bow, though; they'll never tell.

For example:

A present with dimensions 2x3x4 requires 2+2+3+3 = 10 feet of ribbon to wrap the present plus 2*3*4 = 24 feet of ribbon for the bow, for a total of 34 feet.
A present with dimensions 1x1x10 requires 1+1+1+1 = 4 feet of ribbon to wrap the present plus 1*1*10 = 10 feet of ribbon for the bow, for a total of 14 feet.
How many total feet of ribbon should they order?
*/
func partTwo() {
	f, err := os.Open("input.txt")
	if err != nil {
		log.Fatal(err)
	}

	total := 0
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		vals := strings.Split(line, "x")
		valOne, errOne := strconv.ParseInt(vals[0], 10, 32)
		valTwo, errTwo := strconv.ParseInt(vals[1], 10, 32)
		valThr, errThr := strconv.ParseInt(vals[2], 10, 32)

		checkErr(errOne)
		checkErr(errTwo)
		checkErr(errThr)

		firstTwo := 2*valOne + 2*valTwo
		secondTwo := 2*valTwo + 2*valThr
		firstLast := 2*valOne + 2*valThr

		slack := firstTwo
		if secondTwo < slack {
			slack = secondTwo
		}
		if firstLast < slack {
			slack = firstLast
		}

		total += int(valOne*valTwo*valThr + slack)
	}

	fmt.Printf("Total length of ribbon: %v\n", total)
}

func checkErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
