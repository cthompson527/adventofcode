package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path"
	"strconv"
	"strings"
)

func main() {
	valA := partOne()
	fmt.Printf("partOne: %v\n", valA)
	valA = partTwo()
	fmt.Printf("partTwo: %v\n", valA)
}

/*
--- Day 7: Some Assembly Required ---
This year, Santa brought little Bobby Tables a set of wires and bitwise logic gates! Unfortunately, little Bobby is a little under the recommended age range, and he needs help assembling the circuit.

Each wire has an identifier (some lowercase letters) and can carry a 16-bit signal (a number from 0 to 65535). A signal is provided to each wire by a gate, another wire, or some specific value. Each wire can only get a signal from one source, but can provide its signal to multiple destinations. A gate provides no signal until all of its inputs have a signal.

The included instructions booklet describes how to connect the parts together: x AND y -> z means to connect wires x and y to an AND gate, and then connect its output to wire z.

For example:

123 -> x means that the signal 123 is provided to wire x.
x AND y -> z means that the bitwise AND of wire x and wire y is provided to wire z.
p LSHIFT 2 -> q means that the value from wire p is left-shifted by 2 and then provided to wire q.
NOT e -> f means that the bitwise complement of the value from wire e is provided to wire f.
Other possible gates include OR (bitwise OR) and RSHIFT (right-shift). If, for some reason, you'd like to emulate the circuit instead, almost all programming languages (for example, C, JavaScript, or Python) provide operators for these gates.

For example, here is a simple circuit:

123 -> x
456 -> y
x AND y -> d
x OR y -> e
x LSHIFT 2 -> f
y RSHIFT 2 -> g
NOT x -> h
NOT y -> i
After it is run, these are the signals on the wires:

d: 72
e: 507
f: 492
g: 114
h: 65412
i: 65079
x: 123
y: 456
In little Bobby's kit's instructions booklet (provided as your puzzle input), what signal is ultimately provided to wire a?
*/
func partOneFail() { // didn't understand how the wire gate works :/
	f, err := os.Open("input.txt")
	checkErr(err)

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		tokenizer := NewTokenizer(scanner.Text())
		tokenizer.Compute()
		tokenizer.Save()
		log.Printf("%v\n", tokenizer)
	}
	log.Printf("%v\n", wireMap["hh"])
}

var wireMap = map[string]uint16{}

type Operands int

var operators = map[string]bool{
	"AND":    true,
	"NOT":    true,
	"OR":     true,
	"LSHIFT": true,
	"RSHIFT": true,
}

const (
	WIRE     Operands = 1
	OPERATOR Operands = 2
	VALUE    Operands = 3
)

type Tokenizer struct {
	result    string
	resultVal uint16
	operator  string
	operands  []string
}

func NewTokenizer(str string) *Tokenizer {
	assignmentSplit := strings.Split(str, "->")
	operandsSplit := strings.Split(strings.TrimSpace(assignmentSplit[0]), " ")

	operator, operands := func() (string, []string) {
		for i, op := range operandsSplit {
			if operators[op] {
				return op, append(operandsSplit[:i], operandsSplit[i+1:]...)
			}
		}

		return "NONE", operandsSplit
	}()

	return &Tokenizer{
		result:   strings.TrimSpace(assignmentSplit[1]),
		operator: operator,
		operands: operands,
	}
}

func (t *Tokenizer) Compute() {
	operandsTypes := map[string]Operands{}
	// get operands types
	for _, op := range t.operands {
		_, err := strconv.ParseInt(op, 10, 16)
		if err != nil {
			operandsTypes[op] = WIRE
		} else {
			operandsTypes[op] = VALUE
		}
	}
	switch t.operator {
	case "NONE":
		val := GetOperandValue(t.operands[0], operandsTypes)
		t.resultVal = val
	case "NOT":
		val := GetOperandValue(t.operands[0], operandsTypes)
		t.resultVal = ^val
	case "AND":
		val1 := GetOperandValue(t.operands[0], operandsTypes)
		val2 := GetOperandValue(t.operands[1], operandsTypes)
		t.resultVal = val1 & val2
	case "OR":
		val1 := GetOperandValue(t.operands[0], operandsTypes)
		val2 := GetOperandValue(t.operands[1], operandsTypes)
		t.resultVal = val1 | val2
	case "LSHIFT":
		val1 := GetOperandValue(t.operands[0], operandsTypes)
		val2 := GetOperandValue(t.operands[1], operandsTypes)
		t.resultVal = val1 << uint(val2)
	case "RSHIFT":
		val1 := GetOperandValue(t.operands[0], operandsTypes)
		val2 := GetOperandValue(t.operands[1], operandsTypes)
		t.resultVal = val1 >> uint(val2)
	}
}

func (t *Tokenizer) Save() {
	wireMap[t.result] = t.resultVal
}

func GetOperandValue(str string, opTypes map[string]Operands) uint16 {
	if opTypes[str] == WIRE {
		return GetWireValue(str)
	}
	v, err := strconv.ParseInt(str, 10, 16)
	checkErr(err)
	return uint16(v)
}

func GetWireValue(w string) uint16 {
	if _, ok := wireMap[w]; !ok {
		wireMap[w] = 0
		return 0
	}
	return wireMap[w]
}

func checkErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

var cmds = map[string]string{}
var values = map[string]uint16{}

func partOne() uint16 {
	f, err := os.Open("input.txt"))
	checkErr(err)

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		splitArr := strings.Split(scanner.Text(), " -> ")
		cmds[splitArr[1]] = splitArr[0]
	}

	return getValue("a")
	// fmt.Printf("e: %v\n", getValue("e"))
	// fmt.Printf("f: %v\n", getValue("f"))
	// fmt.Printf("g: %v\n", getValue("g"))
	// fmt.Printf("h: %v\n", getValue("h"))
	// fmt.Printf("i: %v\n", getValue("i"))
	// fmt.Printf("x: %v\n", getValue("x"))
	// fmt.Printf("y: %v\n", getValue("y"))
}

func getValue(str string) uint16 {
	// log.Printf("%s -> %s\n", str, cmds[str])
	v, err := strconv.ParseInt(str, 10, 32)
	if err == nil {
		// log.Printf("\treturning %s\n", str)
		return uint16(v)
	}

	if v, ok := values[str]; ok {
		return v
	}

	cmd := strings.Split(cmds[str], " ")
	operator, operands := func() (string, []string) {
		for i, op := range cmd {
			if operators[op] {
				return op, append(cmd[:i], cmd[i+1:]...)
			}
		}

		return "NONE", cmd
	}()

	switch operator {
	case "NOT":
		values[str] = uint16(^getValue(operands[0]))
	case "AND":
		values[str] = uint16(getValue(operands[0]) & getValue(operands[1]))
	case "OR":
		values[str] = uint16(getValue(operands[0]) | getValue(operands[1]))
	case "LSHIFT":
		values[str] = uint16(getValue(operands[0]) << getValue(operands[1]))
	case "RSHIFT":
		values[str] = uint16(getValue(operands[0]) >> getValue(operands[1]))
	default:
		values[str] = uint16(getValue(operands[0]))
	}

	return values[str]
}

/*
--- Part Two ---
Now, take the signal you got on wire a, override wire b to that signal, and reset the other wires (including wire a). What new signal is ultimately provided to wire a?
*/
func partTwo() uint16 {
	valA := partOne()
	values = make(map[string]uint16)
	values["b"] = valA
	return partOne()
}
