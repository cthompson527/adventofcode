package main

import (
	"bufio"
	"log"
	"os"
	"strings"
)

func main() {
	partOne()
	partTwo()
}

/*
--- Day 5: Doesn't He Have Intern-Elves For This? ---
Santa needs help figuring out which strings in his text file are naughty or nice.

A nice string is one with all of the following properties:

It contains at least three vowels (aeiou only), like aei, xazegov, or aeiouaeiouaeiou.
It contains at least one letter that appears twice in a row, like xx, abcdde (dd), or aabbccdd (aa, bb, cc, or dd).
It does not contain the strings ab, cd, pq, or xy, even if they are part of one of the other requirements.
For example:

ugknbfddgicrmopn is nice because it has at least three vowels (u...i...o...), a double letter (...dd...), and none of the disallowed substrings.
aaa is nice because it has at least three vowels and a double letter, even though the letters used by different rules overlap.
jchzalrnumimnmhp is naughty because it has no double letter.
haegwjzuvuyypxyu is naughty because it contains the string xy.
dvszwmarrgswjxmb is naughty because it contains only one vowel.
How many strings are nice?
*/
func partOne() {
	f, err := os.Open("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	scanner := bufio.NewScanner(f)

	niceStrings := 0
	for scanner.Scan() {
		line := scanner.Text()
		if countVowels(line) < 3 || !hasDoubleLetter(line) || hasSubstring(line) {
			continue
		}

		niceStrings++
	}

	log.Printf("Number of nice strings: %d\n", niceStrings)
}

func countVowels(str string) int {
	vowelCount := 0
	for _, char := range str {
		if char == 'a' || char == 'e' || char == 'i' || char == 'o' || char == 'u' {
			vowelCount++
		}
	}

	return vowelCount
}

func hasDoubleLetter(str string) bool {
	for pos := range str {
		if pos == 0 {
			continue
		}

		if str[pos-1] == str[pos] {
			return true
		}
	}

	return false
}

func hasSubstring(str string) bool {
	if strings.Index(str, "ab") != -1 {
		return true
	}

	if strings.Index(str, "cd") != -1 {
		return true
	}

	if strings.Index(str, "pq") != -1 {
		return true
	}

	if strings.Index(str, "xy") != -1 {
		return true
	}

	return false
}

/*
--- Part Two ---
Realizing the error of his ways, Santa has switched to a better model of determining whether a string is naughty or nice. None of the old rules apply, as they are all clearly ridiculous.

Now, a nice string is one with all of the following properties:

It contains a pair of any two letters that appears at least twice in the string without overlapping, like xyxy (xy) or aabcdefgaa (aa), but not like aaa (aa, but it overlaps).
It contains at least one letter which repeats with exactly one letter between them, like xyx, abcdefeghi (efe), or even aaa.
For example:

qjhvhtzxzqqjkmpb is nice because is has a pair that appears twice (qj) and a letter that repeats with exactly one letter between them (zxz).
xxyxx is nice because it has a pair that appears twice and a letter that repeats with one between, even though the letters used by each rule overlap.
uurcxstgmygtbstg is naughty because it has a pair (tg) but no repeat with a single letter between them.
ieodomkazucvgmuy is naughty because it has a repeating letter with one between (odo), but no pair that appears twice.
How many strings are nice under these new rules?
*/
func partTwo() {
	f, err := os.Open("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	scanner := bufio.NewScanner(f)

	niceStrings := 0
	for scanner.Scan() {
		line := scanner.Text()
		if countDoubles(line) && hasSandwich(line) {
			niceStrings++
		}
	}

	log.Printf("Number of nice strings: %d\n", niceStrings)
}

func countDoubles(str string) bool {
	pairs := make(map[string]int)
	lastIndex := make(map[string]int)

	for pos := range str {
		if pos == 0 {
			continue
		}
		key := string([]byte{str[pos-1], str[pos]})
		if _, ok := pairs[key]; ok {
			if lastIndex[key] != pos-1 {
				return true
			}
			continue
		} else {
			pairs[key] = 1
			lastIndex[key] = pos
		}
	}
	return false
}

func hasSandwich(str string) bool {
	for pos := range str {
		if pos < 2 {
			continue
		}

		if str[pos-2] == str[pos] {
			return true
		}
	}
	return false
}
