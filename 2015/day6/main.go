package main

import (
	"bufio"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	partOne()
	partTwo()
}

/*
--- Day 6: Probably a Fire Hazard ---
Because your neighbors keep defeating you in the holiday house decorating contest year after year, you've decided to deploy one million lights in a 1000x1000 grid.

Furthermore, because you've been especially nice this year, Santa has mailed you instructions on how to display the ideal lighting configuration.

Lights in your grid are numbered from 0 to 999 in each direction; the lights at each corner are at 0,0, 0,999, 999,999, and 999,0. The instructions include whether to turn on, turn off, or toggle various inclusive ranges given as coordinate pairs. Each coordinate pair represents opposite corners of a rectangle, inclusive; a coordinate pair like 0,0 through 2,2 therefore refers to 9 lights in a 3x3 square. The lights all start turned off.

To defeat your neighbors this year, all you have to do is set up your lights by doing the instructions Santa sent you in order.

For example:

turn on 0,0 through 999,999 would turn on (or leave on) every light.
toggle 0,0 through 999,0 would toggle the first line of 1000 lights, turning off the ones that were on, and turning on the ones that were off.
turn off 499,499 through 500,500 would turn off (or leave off) the middle four lights.
After following the instructions, how many lights are lit?
*/
func partOne() {
	f, err := os.Open("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	scanner := bufio.NewScanner(f)

	grid := initGrid()
	for scanner.Scan() {
		line := scanner.Text()

		verb, startString, endString := splitSections(line)
		startCoords := createCoords(startString)
		endCoords := createCoords(endString)

		for i := startCoords.x; i <= endCoords.x; i++ {
			for j := startCoords.y; j <= endCoords.y; j++ {
				switch verb {
				case "toggle":
					grid[i][j] = !grid[i][j]
				case "on":
					grid[i][j] = true
				case "off":
					grid[i][j] = false
				}
			}
		}
	}

	log.Printf("Number of lights on: %d\n", countLights(grid))
}

func splitSections(str string) (string, string, string) {
	cmdArray := strings.Split(str, " ")
	if cmdArray[0] == "turn" {
		return cmdArray[1], cmdArray[2], cmdArray[4]
	}
	return cmdArray[0], cmdArray[1], cmdArray[3]
}

type coords struct {
	x int
	y int
}

func createCoords(str string) coords {
	strArr := strings.Split(str, ",")
	xInt, err := strconv.ParseInt(strArr[0], 10, 32)
	if err != nil {
		log.Fatal(err)
	}
	yInt, err := strconv.ParseInt(strArr[1], 10, 32)
	if err != nil {
		log.Fatal(err)
	}
	return coords{
		x: int(xInt),
		y: int(yInt),
	}
}

func initGrid() [1000][1000]bool {
	return [1000][1000]bool{{false}}
}

func countLights(grid [1000][1000]bool) int {
	count := 0
	for i := 0; i < 1000; i++ {
		for j := 0; j < 1000; j++ {
			if grid[i][j] {
				count++
			}
		}
	}
	return count
}

/*
--- Part Two ---
You just finish implementing your winning light pattern when you realize you mistranslated Santa's message from Ancient Nordic Elvish.

The light grid you bought actually has individual brightness controls; each light can have a brightness of zero or more. The lights all start at zero.

The phrase turn on actually means that you should increase the brightness of those lights by 1.

The phrase turn off actually means that you should decrease the brightness of those lights by 1, to a minimum of zero.

The phrase toggle actually means that you should increase the brightness of those lights by 2.

What is the total brightness of all lights combined after following Santa's instructions?

For example:

turn on 0,0 through 0,0 would increase the total brightness by 1.
toggle 0,0 through 999,999 would increase the total brightness by 2000000.

*/
func partTwo() {
	f, err := os.Open("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	scanner := bufio.NewScanner(f)

	grid := initGridBrightness()
	for scanner.Scan() {
		line := scanner.Text()
		verb, startString, endString := splitSections(line)
		startCoords := createCoords(startString)
		endCoords := createCoords(endString)

		for i := startCoords.x; i <= endCoords.x; i++ {
			for j := startCoords.y; j <= endCoords.y; j++ {
				switch verb {
				case "toggle":
					grid[i][j] += 2
				case "on":
					grid[i][j]++
				case "off":
					grid[i][j]--
					if grid[i][j] < 0 {
						grid[i][j] = 0
					}
				}
			}
		}
	}
	log.Printf("Total brightness: %d\n", countLightsBrightness(grid))
}

func initGridBrightness() [1000][1000]int {
	return [1000][1000]int{{0}}
}

func countLightsBrightness(grid [1000][1000]int) int {
	count := 0
	for i := 0; i < 1000; i++ {
		for j := 0; j < 1000; j++ {
			count += grid[i][j]
		}
	}
	return count
}
