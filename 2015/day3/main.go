package main

import (
	"fmt"
	"io/ioutil"
	"log"
)

func main() {
	partOne()
	partTwo()
}

/*
--- Day 3: Perfectly Spherical Houses in a Vacuum ---
Santa is delivering presents to an infinite two-dimensional grid of houses.

He begins by delivering a present to the house at his starting location, and then an elf at the North Pole calls him via radio and tells him where to move next. Moves are always exactly one house to the north (^), south (v), east (>), or west (<). After each move, he delivers another present to the house at his new location.

However, the elf back at the north pole has had a little too much eggnog, and so his directions are a little off, and Santa ends up visiting some houses more than once. How many houses receive at least one present?

For example:

> delivers presents to 2 houses: one at the starting location, and one to the east.
^>v< delivers presents to 4 houses in a square, including twice to the house at his starting/ending location.
^v^v^v^v^v delivers a bunch of presents to some very lucky children at only 2 houses.
*/
func partOneFail() { // FAIL!!
	directions := []byte("^>v<")

	root := &node{
		above: nil,
		below: nil,
		right: nil,
		left:  nil,
		// visited: true,
	}

	currentNode := root
	count := 1

	for _, dir := range directions {
		switch fmt.Sprintf("%c", dir) {
		case ">":
			if currentNode.right == nil {
				currentNode.right = newNode()
				currentNode.right.left = currentNode
				count++
			}
			currentNode = currentNode.right
		case "<":
			if currentNode.left == nil {
				currentNode.left = newNode()
				currentNode.left.right = currentNode
				count++
			}
			currentNode = currentNode.left
		case "^":
			if currentNode.above == nil {
				currentNode.above = newNode()
				currentNode.above.below = currentNode
				count++
			}
			currentNode = currentNode.above
		case "v":
			if currentNode.below == nil {
				currentNode.below = newNode()
				currentNode.below.above = currentNode
				count++
			}
			currentNode = currentNode.below
		}
	}
	fmt.Printf("num of houses: %v\n", count)

}

type node struct {
	above *node
	below *node
	right *node
	left  *node
	// visited bool
}

func newNode() *node {
	return &node{
		above: nil,
		below: nil,
		right: nil,
		left:  nil,
		// visited: false,
	}
}

func partOne() {
	directions, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}

	count := 1
	var history = make([]coords, 10)
	location := coords{x: 0, y: 0}

	for _, dir := range directions {
		switch fmt.Sprintf("%c", dir) {
		case ">":
			location.x++
		case "<":
			location.x--
		case "^":
			location.y++
		case "v":
			location.y--
		}
		if !isInHistory(history, location) {
			count++
			history = append(history, location)
		}
	}
	fmt.Printf("num of houses: %v\n", count)
}

type coords struct {
	x int64
	y int64
}

func isInHistory(his []coords, c coords) bool {
	for _, h := range his {
		if h.x == c.x && h.y == c.y {
			return true
		}
	}
	return false
}

/*
--- Part Two ---
The next year, to speed up the process, Santa creates a robot version of himself, Robo-Santa, to deliver presents with him.

Santa and Robo-Santa start at the same location (delivering two presents to the same starting house), then take turns moving based on instructions from the elf, who is eggnoggedly reading from the same script as the previous year.

This year, how many houses receive at least one present?

For example:

^v delivers presents to 3 houses, because Santa goes north, and then Robo-Santa goes south.
^>v< now delivers presents to 3 houses, and Santa and Robo-Santa end up back where they started.
^v^v^v^v^v now delivers presents to 11 houses, with Santa going one direction and Robo-Santa going the other.
*/
func partTwo() {
	directions, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}

	count := 1
	var history = make([]coords, 10)
	santaLocation := coords{x: 0, y: 0}
	roboLocation := coords{x: 0, y: 0}

	for index, dir := range directions {
		switch fmt.Sprintf("%c", dir) {
		case ">":
			if index%2 == 0 {
				santaLocation.x++
			} else {
				roboLocation.x++
			}
		case "<":
			if index%2 == 0 {
				santaLocation.x--
			} else {
				roboLocation.x--
			}
		case "^":
			if index%2 == 0 {
				santaLocation.y++
			} else {
				roboLocation.y++
			}
		case "v":
			if index%2 == 0 {
				santaLocation.y--
			} else {
				roboLocation.y--
			}
		}
		if index%2 == 0 {
			if !isInHistory(history, santaLocation) {
				count++
				history = append(history, santaLocation)
			}
		} else {
			if !isInHistory(history, roboLocation) {
				count++
				history = append(history, roboLocation)
			}
		}
	}
	fmt.Printf("num of houses: %v\n", count)
}
